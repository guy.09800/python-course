import calendar

# 4.2.2
word = input("Enter a word to check if its a palindrome: \n").lower().replace(" ","")
print(word == word[::-1])
# 4.2.3
temp = input("Insert the temperature you want to convert: \n").lower()
if temp.endswith('c'):
    temp = float(''.join(i for i in temp if i.isdigit()))
    temp = (temp*9 + (32*5))/5
else:
    temp = float(''.join(i for i in temp if i.isdigit()))
    temp = (temp * 5 - 160) / 9

print(temp)
# 4.2.4
date = input("Enter a data with the following format: dd/mm/yyyy \n")
date = date.split("/")
wday = ['Monday', "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
print(wday[calendar.weekday(int(date[2]),int(date[1]),int(date[0]))])

# 4.3.1

letter = input("Guess a letter: ")

if(len(letter) != 1 and not letter.isalpha()):
    print("E3")
elif(len(letter) != 1):
    print("E1")
elif(not letter.isalpha()):
    print("E2")
else:
    print(letter)
