# 7.1.4
def squared_numbers(start : int, stop : int) -> list:
    return [num*num for num in range(start, stop+1)]

# 7.2.1
def is_greater(my_list : list, n) -> list:
    return [num for num in my_list if num > n]

# 7.2.2
def numbers_letters_count(my_str : str) -> list:
    num_count = sum(1 for char in my_str if char.isdigit())
    letter_count = sum(1 for char in my_str if char.isalpha())
    return [num_count, letter_count]

# 7.2.4
def seven_boom(end_number):
    result = []
    for num in range(end_number+1):
        if num == 0 or num % 7 == 0 or '7' in str(num):
            result.append("boom")
        else:
            result.append(num)
    return result

# 7.2.5
def sequence_del(my_str):
    new_str = ""
    for i in range(len(my_str)):
        if i == 0 or my_str[i] != my_str[i-1]:
            new_str += my_str[i]
    return new_str

# 7.2.6
def shopping_list():
    shopping_cart = input("Enter groceries")
    groceries_list = shopping_cart.split(',')
    select = 0
    while(select != 9):
        if(select == 1):
            print(groceries_list)
        if(select == 2):
            print(len(groceries_list))
        if(select == 3):
            item = input("enter an item to check if its the list")
            print(item in groceries_list)
        if(select == 4):
            item = input("Enter an item name to check how many times it appears in the list")
            print(groceries_list.count('item'))
        if(select == 5):
            item = input("Enter an item to remove")
            groceries_list.remove(item)
        if(select == 6):
            item = input("Enter an item to add")
            groceries_list.append(item)
        if(select == 7):
            for grocery in groceries_list:
                if len(grocery) < 3 and not grocery.isalpha():
                    print(grocery)
        if(select == 8):
            grocery_list = list(set(grocery_list))

        if(select == 9):
            break
        


# 7.2.7
def arrow(my_char, max_length):
    for i in range(1, max_length + 1):
        print(my_char * i)
    for i in range(max_length - 1, 0, -1):
        print(my_char * i)

# 7.3.1
def show_hidden_word(secret_word : str, old_letters_guessed : list):
    hidden_word = ''
    for letter in secret_word:
        if letter in old_letters_guessed:
            hidden_word += letter
        else:
            hidden_word += '_'
        hidden_word += ' '
    return hidden_word.strip()

# 7.3.2
def check_win(secret_word : str, old_letters_guessed: list) -> bool:
     for letter in secret_word:
        if letter not in old_letters_guessed:
            return False
     return True