# 3.2.1
print('"Shuffle, Shuffle, Shuffle", say it together!\n'+ "Change colors and directions,\n" + "Don't back down and stop the player!\n" + "\tDo you want to play Taki?\n"+"\tPress y\\n")

# 3.3.3
encrypted_message = "!XgXnXiXcXiXlXsX XnXoXhXtXyXpX XgXnXiXnXrXaXeXlX XmXaX XI"
print(encrypted_message[::-2])

#3.4.2
string = input("Please enter a string: ")
first_letter = string[0]
print(first_letter + string[1::1].replace(first_letter, 'e'))

# 3.4.3
string = input("Please enter a string: ")
middle_point = len(string)//2
print(string[0:middle_point] + string[middle_point:len(string)].upper())