# 5.3.4
def last_early(my_str):
    return my_str[-1:] in my_str[:-1]

print(last_early("best of luck"))

# 5.3.5
def distance(num1, num2, num3):
    if(((abs(num2 - num1) <= 1 or abs(num3 - num1) <= 1) and
        ((abs(num2 - num1) >= 2 and abs(num2 - num3) >= 2) or (abs(num3 - num1) >= 2 and abs(num3 - num1) >= 2)))):
        return True
    else:
        return False
print(distance(4,5,3))

# 5.3.6
def filter_teens(a = 13, b = 13 , c = 13):
    return fix_ages(a) + fix_ages(b) + fix_ages(c)

def fix_ages(age):
    if( age > 19 or age < 13 or 15 <= age <= 16):
        return age
    else:
        return 0

print(filter_teens(2,13,1))

# 5.3.7
def chocolate_maker(small, big, x):

    big_needed = min(x // 5, big) 
    small_needed = x - big_needed * 5  
    
    return small_needed <= small
# 5.4.1
def func(num1, num2):
    if abs(num1 - num2) == 1:
        return True
    else:
        return False

# 5.5.1

def is_valid_input(letter_guessed):
    if(len(letter_guessed)) < 2 and str(letter_guessed).isalpha():
        return True
    return False
        