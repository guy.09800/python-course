# 6.1.2
def shift_left(my_list):

    if len(my_list) < 2:
        return my_list  

    first_elem = my_list[0]
    shifted_list = my_list[1:]
    shifted_list.append(first_elem)
    
    return shifted_list

# 6.2.3
def format_list(my_list):
    even_list = my_list[::2]
    last_item = my_list[-1] if len(my_list) % 2 == 0 else ''
    
    if len(even_list) == 0:
        return last_item
    
    elif len(even_list) == 1:
        return even_list[0] + ', ' + last_item
    
    else:
        formatted_list = ', '.join(even_list[:-1]) + ', and ' + even_list[-1]
        if last_item:
            formatted_list += ', ' + last_item
        return formatted_list

my_list = ["hydrogen", "helium", "lithium", "beryllium", "boron", "magnesium"]
print(format_list(my_list))

# 6.2.4
def extend_list_x(list_x, list_y):
    for item in list_y:
        list_x.append(item)

# 6.3.1
def are_lists_equal(list1, list2):
    if len(list1) != len(list2):
        return False
    
    for item in list1:
        if item not in list2:
            return False
    
    for item in list2:
        if item not in list1:
            return False
    
    return True

# 6.3.2

def longest(my_list):
    return max(my_list, key=len)

# 6.4.1

def check_valid_input(letter_guessed: str, old_letters_guessed: list) -> bool:
    if len(letter_guessed) >= 2 or not letter_guessed.isalpha or letter_guessed.lower() in old_letters_guessed:
        return False
    return True
# 6.4.2
def try_update_letter_guessed(letter_guessed : str, old_letters_guessed) -> bool:
    if check_valid_input(letter_guessed, old_letters_guessed):
        old_letters_guessed.append(letter_guessed.lower())
        return True
    else:
        print("X")
        print(" -> ".join(sorted(old_letters_guessed, key=str.lower)[::1]))
        return False
