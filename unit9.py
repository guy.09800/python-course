# 9.1.1
def are_files_equal(file1, file2) -> bool:
    with open(file1, 'r') as f1, open(file2, 'r') as f2:
        return f1.read() == f2.read()

# 9.1.2
file_path = input("Enter a file path: ")
task = input("Enter a task: ")
if task == "sort":
    word_list = []
    with open(file_path, 'r') as file:
        for line in file.readlines():
            for word in line.split():
                if word not in word_list:
                    word_list.append(word)
    print(sorted(word_list))
    
    if task == "rev":
        with open(file_path, 'r') as file:
            lines = file.readlines()
            for line in reversed(lines):
                print(line.strip()[::-1])
    
    if task == "last":
        with open(file_path, 'r') as file:
            n = int(input("Enter the number of lines to print from the end of the file: "))
            lines = file.readlines()
            for line in reversed(lines[-n:]):
                print(line.strip())

# 9.2.2
def copy_file_content(source, destination):
    with open(source, 'r') as f1, open(destination, 'w') as f2:
        data = f1.read()
        f2.write(data)

# 9.2.3
def who_is_missing(file_name):
    with open(file_name, 'r') as file:
        numbers = file.read()
    numbers = numbers.replace(",", "")
    max_num = max(numbers)
    for num in range(1, int(max_num) + 1):
        if str(num) not in numbers:
            missing_num = num
            break
    with open("found.txt", 'w') as output:
        output.write(str(missing_num))
    return missing_num

# 9.3.1
def my_mp3_playlist(file_path):
    song_list = []
    with open(file_path, 'r') as file:
        for line in file.readlines():
            song_list.append(line.strip().split(";"))
    biggest_num = 0
    longest_song = ""
    for item in song_list:
        if int(item[2].split(':')[0]) * 60 + int(item[2].split(':')[1]) > biggest_num:
            biggest_num = int(item[2].split(':')[0]) * 60 + int(item[2].split(':')[1])
            longest_song = item[0]

    artist_dict = {}
    for item in song_list:
        artist = item[1]
        if artist not in artist_dict:
            artist_dict[artist] = 1
        else:
            artist_dict[artist] += 1

    most_songs_artist = max(artist_dict, key=artist_dict.get)
    mp3_tuple = (longest_song, len(song_list), most_songs_artist)
    return mp3_tuple


# 9.3.2
def my_mp4_playlist(file_path, new_song):
    with open(file_path, 'r') as file:
        lines = file.readlines()
        if len(lines) < 3:
            lines.extend(['\n'] * (3 - len(lines)))
        lines[2] = new_song + lines[2][lines[2].find(';'):]
    with open(file_path, 'w') as file:
        file.writelines(lines)
    with open(file_path, 'r') as file:
        print(file.read())