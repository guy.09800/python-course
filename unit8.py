# 8.2.1
data = ("self", "py", 1.543)
format_string = "Hello %s.%s learner, you have only %.1f units left before you master the course!"

print(format_string % data)

# 8.2.2

def sort_items_by_price(item_list:list) ->list:
    sorted_list = sorted(item_list, key=lambda x: x[1], reverse=True)
    return sorted_list

# 8.2.3

def mult_tuple(tup1, tup2):
    pairs = [(x, y) for x in tup1 for y in tup2]
    return tuple(pairs)

# 8.2.4

def sort_anagrams(list_of_strings : list) -> list:
    anagram_groups = {}
    for word in list_of_strings:
        key = ''.join(sorted(word))
        if key in anagram_groups:
            anagram_groups[key].append(word)
        else:
            anagram_groups[key] = [word]
    sorted_lists = [anagram_groups[key] for key in sorted(anagram_groups.keys())]
    return sorted_lists


# 8.3.2
my_dict = {
    "first_name" : "Mariah",
    "last_name" : "Carey",
    "birth_date" : "27.03.1970",
    "hobbies" : ["sing","compose","act"]
}

selection = input("Enter a number 1-7: ")

if selection.isdigit():
    selection = int(selection)
    if selection == 1:
        print(my_dict["last_name"])
    elif selection == 2:
        print(my_dict["birth_date"].split(".")[1])
    elif selection == 3:
        print(len(my_dict["hobbies"]))
    elif selection == 4:
        print(my_dict["hobbies"][-1])
    elif selection == 5:
        my_dict["hobbies"].append("cooking")
        print(my_dict)
    elif selection == 6:
        birthdate_list = my_dict["birth_date"].split(".")
        my_dict["birth_date"] = tuple(birthdate_list)
        print(my_dict)
    elif selection == 7:
        if type(my_dict["birth_date"]) == str:
            agelist = my_dict["birth_date"].split(".")
            my_dict["age"] = 2023 - int(agelist[2])
            print(my_dict["age"])
        else:
            my_dict["age"] = 2023 - int(my_dict["birth_date"][2])
            print(my_dict["age"])
    else:
        print("Invalid input. Please enter a number from 1 to 7.")
else:
    print("Invalid input. Please enter a number from 1 to 7.")


# 8.3.3
def count_chars(my_str):
    char_count = {}
    for char in my_str:
        if char not in char_count:
            char_count[char] = 1
        else:
            char_count[char] += 1
    return char_count

# 8.3.4
def inverse_dict(d):
    return {v: k for k, v in d.items()}

# 8.4.1

MAX_TRIES = 6
VALID_LETTERS = "abcdefghijklmnopqrstuvwxyz"
HANGMAN_PHOTOS = {
    0: """    x-------x""",
    1: """    x-------x
    |
    |
    |
    |
    |""",
    2: """    x-------x
    |       |
    |       0
    |
    |
    |""",
    3: """    x-------x
    |       |
    |       0
    |       |
    |
    |""",
    4: """    x-------x
    |       |
    |       0
    |      /|\\
    |
    |""",
    5: """    x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |""",
    6: """    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |"""
}

