import random as rand

HANGMAN_ASCII_ART = """Welcome to the game Hangman
  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                      __/ |                      
                     |___/\n"""
MAX_TRIES = 6

VALID_LETTERS = "abcdefghijklmnopqrstuvwxyz"

HANGMAN_PHOTOS = {
    0: """    x-------x""",
    1: """    x-------x
    |
    |
    |
    |
    |""",
    2: """    x-------x
    |       |
    |       0
    |
    |
    |""",
    3: """    x-------x
    |       |
    |       0
    |       |
    |
    |""",
    4: """    x-------x
    |       |
    |       0
    |      /|\\
    |
    |""",
    5: """    x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |""",
    6: """    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |"""
}

def print_hangman(num_of_tries : int):
    print(HANGMAN_PHOTOS[num_of_tries])

def choose_word(file_path, index : int) -> tuple:
    word_list = []
    with open(file_path) as file:
        for line in file.readlines():
            for word in line.split():
                word_list.append(word)
    num_of_duplicates = 0
    for element in word_list:
        if word_list.count(element) > 1:
            num_of_duplicates += word_list.count(element) - 1
            
    if index > len(word_list):
        if index % len(word_list) != 0:
            index = (index % len(word_list))
        else:
            index = len(word_list) 

    output_tuple = (len(word_list) - num_of_duplicates, word_list[index -1])
    return output_tuple


def check_valid_input(letter_guessed: str, old_letters_guessed: list) -> bool:
    if len(letter_guessed) >= 2 or not letter_guessed.isalpha or letter_guessed.lower() in old_letters_guessed:
        return False
    return True


def try_update_letter_guessed(letter_guessed : str, old_letters_guessed) -> bool:
    if check_valid_input(letter_guessed, old_letters_guessed):
        old_letters_guessed.append(letter_guessed.lower())
        return True
    else:
        print("X")
        print(" -> ".join(sorted(old_letters_guessed, key=str.lower)[::1]))
        return False




def show_hidden_word(secret_word : str, old_letters_guessed : list) -> str:
    hidden_word = ''
    for letter in secret_word:
        if letter in old_letters_guessed:
            hidden_word += letter
        else:
            hidden_word += '_'
        hidden_word += ' '
    return hidden_word.strip()

def check_win(secret_word : str, old_letters_guessed : list) -> bool:
    for letter in secret_word:
        if letter not in old_letters_guessed:
            return False
    return True


# Main:

print(HANGMAN_ASCII_ART + str(MAX_TRIES)) 
file_path = input("Enter file Path: ")
index = int(input("Enter index: "))
secret = choose_word(file_path, index)[1]
attempts = 0
old_letters_guessed = []
while attempts < MAX_TRIES:
    print_hangman(attempts)
    print(show_hidden_word(secret, old_letters_guessed))
    guess = input("Please Enter a letter: ")
    while not try_update_letter_guessed(guess, old_letters_guessed):
        guess = input("Please Enter a letter: ")
    if guess in secret:
        if check_win(secret, old_letters_guessed):
            print(show_hidden_word(secret, old_letters_guessed))
            print("YOU WON")
            break
        else:
            continue
    else:
        print('WRONG')
        attempts += 1
        continue
        
if not check_win(secret, old_letters_guessed):
    print_hangman(attempts)
    print(show_hidden_word(secret, old_letters_guessed))
    print("YOU LOSE")
